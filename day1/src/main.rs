/*
Fuel required to launch a given module is based on its mass. Specifically, to find the fuel
required for a module, take its mass, divide by three, round down, and subtract 2.

For example:

    For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
    For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
    For a mass of 1969, the fuel required is 654.
    For a mass of 100756, the fuel required is 33583.

The Fuel Counter-Upper needs to know the total fuel requirement. To find it, individually calculate
the fuel needed for the mass of each module (your puzzle input), then add together all the fuel values.

What is the sum of the fuel requirements for all of the modules on your spacecraft?

--- Part Two ---

Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and
subtract 2. However, that fuel also requires fuel, and that fuel requires fuel, and so on. Any mass
that would require negative fuel should instead be treated as if it requires zero fuel; the
remaining mass, if any, is instead handled by wishing really hard, which has no mass and is outside
the scope of this calculation.

So, for each module mass, calculate its fuel and add it to the total. Then, treat the fuel amount
you just calculated as the input mass and repeat the process, continuing until a fuel requirement
is zero or negative. For example:

    A module of mass 14 requires 2 fuel. This fuel requires no further fuel (2 divided by 3 and
    rounded down is 0, which would call for a negative fuel), so the total fuel required is still
    just 2.
    At first, a module of mass 1969 requires 654 fuel. Then, this fuel requires 216 more fuel
    (654 / 3 - 2). 216 then requires 70 more fuel, which requires 21 fuel, which requires 5 fuel,
    which requires no further fuel. So, the total fuel required for a module of mass 1969 is 654
    + 216 + 70 + 21 + 5 = 966.
    The fuel required by a module of mass 100756 and its fuel is: 33583 + 11192 + 3728 + 1240 + 411
    + 135 + 43 + 12 + 2 = 50346.

What is the sum of the fuel requirements for all of the modules on your spacecraft when also taking
into account the mass of the added fuel? (Calculate the fuel requirements for each module
separately, then add them all up at the end.)

*/

fn main() {
    let modules = [
        137113, 91288, 62216, 61150, 143536, 69244, 102261, 105683, 58305, 67377, 107379, 108666,
        56279, 123299, 120794, 60286, 112665, 144945, 100039, 60631, 77509, 106891, 103638, 132144,
        119960, 96479, 131631, 105498, 124620, 88703, 101268, 72720, 135531, 108871, 90019, 129257,
        69947, 69968, 104725, 95262, 119107, 111562, 81709, 102441, 129733, 84750, 101748, 107232,
        113844, 115357, 125062, 83869, 69129, 79132, 144282, 115941, 144188, 58559, 92455, 135538,
        146503, 142974, 73517, 112043, 143187, 130617, 144656, 114329, 130205, 92973, 134265,
        120776, 62569, 145143, 131663, 130428, 121409, 109042, 111748, 99222, 102198, 63934,
        130811, 139884, 107805, 107306, 140757, 149374, 119437, 131554, 55182, 69234, 54593, 92531,
        69679, 111405, 143524, 66057, 93150, 53854,
    ];
    // let modules = [100756];
    let sum:i32 = modules.iter()
        .map(|el| process(&el, &0))
        .sum();

    println!("{:?}", sum);
}

fn process(module: &i32, fuel_required: &i32) -> i32 {
    let current_module_fuel_required = get_required_fuel(&module);
    let new_total_fuel_required = match fuel_required.checked_add(current_module_fuel_required) {
        Some(i) => i,
        None => *fuel_required
    };
    
    if current_module_fuel_required >= 0 {
        return process(&current_module_fuel_required, &new_total_fuel_required);
    } else {
        return *fuel_required;
    }
}

fn get_required_fuel(module: &i32) -> i32 {
    return (divide_by_three(&module).trunc() as i32) - 2
}

fn divide_by_three(el: &i32) -> f32 {
    return (el / 3) as f32;
}