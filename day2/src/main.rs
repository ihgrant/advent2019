fn main() {
    for noun in 0..100 {
        for verb in 0..100 {
            let result = execute(noun, verb);
            if result == 19690720 {
                println!("noun is {:?} and verb is {:?}", noun, verb);
                break;
            }
        }
    }
}

fn execute(noun: i32, verb: i32) -> i32 {
    let program = vec![
        1, noun, verb, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 13, 1, 19, 1, 6, 19, 23, 2, 23, 6,
        27, 1, 5, 27, 31, 1, 10, 31, 35, 2, 6, 35, 39, 1, 39, 13, 43, 1, 43, 9, 47, 2, 47, 10, 51,
        1, 5, 51, 55, 1, 55, 10, 59, 2, 59, 6, 63, 2, 6, 63, 67, 1, 5, 67, 71, 2, 9, 71, 75, 1, 75,
        6, 79, 1, 6, 79, 83, 2, 83, 9, 87, 2, 87, 13, 91, 1, 10, 91, 95, 1, 95, 13, 99, 2, 13, 99,
        103, 1, 103, 10, 107, 2, 107, 10, 111, 1, 111, 9, 115, 1, 115, 2, 119, 1, 9, 119, 0, 99, 2,
        0, 14, 0,
    ];
    let result_program = iterate(program, 0);
    return result_program[0];
}

fn iterate(program: Vec<i32>, step: i32) -> Vec<i32> {
    let offset = (step * 4) as usize;
    let opcode = program[offset];

    if opcode == 99 {
        return program;
    }

    let input1_pos = program[offset + 1] as usize;
    let input2_pos = program[offset + 2] as usize;
    let input1 = program[input1_pos];
    let input2 = program[input2_pos];
    let result = operate(opcode, input1, input2);
    let output_pos = program[offset + 3] as usize;

    let new_program = program
        .iter()
        .enumerate()
        .map(|(i, _el)| {
            if i == output_pos {
                return result;
            } else {
                return program[i as usize];
            }
        })
        .collect();

    return iterate(new_program, step + 1);
}

fn operate(opcode: i32, input1: i32, input2: i32) -> i32 {
    return match opcode {
        1 => input1 + input2,
        2 => input1 * input2,
        _ => 0,
    };
}
